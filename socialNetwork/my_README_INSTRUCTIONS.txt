-# To run and operationalize the DeathStarBench application - for further instructions see here: https://github.com/delimitrou/DeathStarBench/tree/master/socialNetwork
cd socialNetwork
docker-compose up -d

# to record data configure Jaeger, ElasticSearch, and Kibana as shown in docker-compose file 

# initialize social network, create users and user-follow-user relationships
python3 scripts/init_social_graph.py

# change to wrk2 directory
cd wrk2/

# execute scripts to sample RPC data over two minutes
python3 myRegularLoginData etc etc

# lookup Jaeger and ElasticSearch to record and download span data

# stop running application
docker-compose down

# docker system prune --volumes

