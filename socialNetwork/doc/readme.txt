######################
###    Run the social network application

docker-compose up -d
python3 scripts/init_social_graph.py


###
# network traffic generator
cd wrk2
# compose posts
./wrk -D exp -t 2 -c 20 -d 2m -L -s ./scripts/social-network/compose-post.lua http://localhost:8080/wrk2-api/post/compose -R 100

# read home timelines
./wrk -D exp -t 2 -c 10 -d 1m -L -s ./scripts/social-network/read-home-timeline.lua http://localhost:8080/wrk2-api/home-timeline/read 50

# read user timelines
./wrk -D exp -t 2 -c 10 -d 1m -L -s ./scripts/social-network/read-user-timeline.lua http://localhost:8080/wrk2-api/user-timeline/read 50

####
curl commands

#  register a user
curl -d "user_id=9012&first_name=stevie&last_name=jayyyk&username=alexx&password=43210" http://192.168.1.15:8080/wrk2-api/user/register


#  user compose a post for the first time
curl -d "user_id=9012&username=alexx&password=43210&post_type=0&text=Holy Grace" http://192.168.1.15:8080/wrk2-api/post/compose
33 spans
(32 spans if not the first time a user wrote a post)


#  send a GET request for user's recent posts
curl "http://192.168.1.15:8080/wrk2-api/user-timeline/read?user_id=9012&start=1&stop=5"
Returns ...
[
{"timestamp":"1589276580351","user_mentions":{},"post_id":"783931535414759424","text":"True Loyalty",
"req_id":"832571528760844416","media":{},"urls":{},"post_type":1,"creator":{"user_id":"9012","username":"alexx"}},
{"timestamp":"1589276566286","user_mentions":{},"post_id":"783931535357157376","text":"Absolute Courage",
"req_id":"335463545013805056","media":{},"urls":{},"post_type":1,"creator":{"user_id":"9012","username":"alexx"}},
{"timestamp":"1589276549297","user_mentions":{},"post_id":"783931535287562240","text":"Delicate Grace",
"req_id":"944071528491602688","media":{},"urls":{},"post_type":1,"creator":{"user_id":"9012","username":"alexx"}},
{"timestamp":"1589275568335","user_mentions":{},"post_id":"783931531269550080","text":"Holy Grace",
"req_id":"526558376788430592","media":{},"urls":{},"post_type":0,"creator":{"user_id":"9012","username":"alexx"}}]



# command to login correctly 
curl "http://192.168.1.15:8080/api/user/login?username=alexx&password=43210"
Note: first time user correctly logs in returns 6 spans; 4 spans otherwise


# following command with missing parameter
curl "http://192.168.1.15:8080/api/user/login?username=alexx�


Returns:
Incomplete arguments
http://192.168.16.17:8080
Note: returns only 2 spans with no error tags


# following command with incorrect password
curl "http://192.168.1.15:8080/api/user/login?username=alexx&password=1234"
User login failure: Incorrect username or password
Note: returns a trace of 4 spans with 2 tagged as errors


######################
###   -    ElasticSearch APIs


First data set for password guessing attack
* register single user
* run api/user/login request 21 times, correct and incorrect logins, groups of consecutive incorrect logins 


##  first 10 spans
http://192.168.1.15:9200/jaeger-span-2020-05-26/_search

##  get all spans
http://192.168.1.15:9200/jaeger-span-2020-05-26/span/_search?q=*&from=0&size=100


# Example:
download bulk data 

es2csv -q '*' -f traceID spanID process.serviceName operationName startTimeMillis startTime tags references -S startTime -u http://localhost:9200 -i jaeger-span-2020-06-29 -D span -o newSpareData/startUpNetwork.csv
Found 343521 results.
Run query [#####################################################################] [343521/343521] [100%] [0:02:15] [Time: 0:02:15] [  2.5 Kidocs/s]
Write to csv [#################################################################] [343521/343521] [100%] [0:01:16] [Time: 0:01:16] [  4.4 Kilines/s]


# Attempt 1

###  download bulk data from elasticsearch
es2csv -q '*' -f traceID spanID process.serviceName operationName references.spanID tags startTimeMillis duration -S startTime -u http://192.168.1.15:9200 -i jaeger-span-2020-05-26 -D span -o data/loginTraceDataV1.csv

* 26/05/2020
loginTraceDataV1.csv


###########################################

# Attempt 2
28/05/2020

* register 960 users, 18800 edges finished to initialize a social graph
startSocialGraphV1.csv


* http workload traffic to compose posts
cd wrk2
./wrk -D exp -t 2 -c 20 -d 1m -L -s ./scripts/social-network/compose-post.lua http://localhost:8080/wrk2-api/post/compose -R 100
6000 requests

metrics
106800 spans
5828 traces
20 s/trace

composePostsV1.csv


* generate successful login HTTP calls used for training & testing
102 spans
25 traces
4 s/trace

normalLoginTraces.csv


* generate unsuccessful login HTTP calls used for training & testing
20 spans
5 traces
4 s/trace

incorrectLoginTraces.csv


###########################################


# Attempt 3
29/05/2020 - 02/06/2020

* register 960 users, 18800 edges finished to initialize a social graph
startSocialGraphV1.csv


* http workload traffic to compose posts
cd wrk2
./wrk -D exp -t 2 -c 20 -d 1m -L -s ./scripts/social-network/compose-post.lua http://localhost:8080/wrk2-api/post/compose -R 100
6000 requests

104690 spans
5994 traces
5856 traces( barring ones with only 1 span)


* run login http call to login correctly 1001 times with 962 users
with 40 additional logins

6224 spans
1001 traces

* run http call to login incorrectly 100 times

401 spans
100 traces


###############################################################################################
Date: 22/09/2020

Microservices Social networking Application
Password attack
Group Anomaly Detection to detect password guessing

Run start up script 

Spans sorted by time  last traceID 
las starting trace: 19307d371c4b8e

