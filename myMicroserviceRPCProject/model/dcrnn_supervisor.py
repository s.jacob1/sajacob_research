import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import sys
import tensorflow as tf
import time
import math
import yaml

from lib import utils, metrics
from lib.AMSGrad import AMSGrad
from lib.metrics import masked_mae_loss

from model.dcrnn_model import DCRNNModel


class DCRNNSupervisor(object):
    """
    Do experiments using Graph Random Walk RNN model.
    """

    def __init__(self, adj_mx, **kwargs):

        self._kwargs = kwargs
        model_number = kwargs.get('model_number')
        self._data_kwargs = kwargs.get('data')
        self._model_kwargs = kwargs.get('model')
        self._train_kwargs = kwargs.get('train')
        self._threshold = 0.0

        # logging.
        self._log_dir = self._get_log_dir(kwargs)
        log_level = self._kwargs.get('log_level', 'INFO')
        self._logger = utils.get_logger(self._log_dir, __name__, "info.log", level=log_level)
        self._writer = tf.summary.FileWriter(self._log_dir)
        self._logger.info(kwargs)

        # Data preparation
        self._logger.info("\n\nData Preparation")
        self._data = utils.load_dataset(model_number, **self._data_kwargs)
        for k, v in self._data.items():
            if hasattr(v, 'shape'):
                self._logger.info((k, v.shape))

        # Build models
        scalar = self._data['scalar']
        with tf.name_scope('Train'):
            with tf.variable_scope('DCRNN', reuse=False):
                self._train_model = DCRNNModel(is_training=True, scalar=scalar,
                                               batch_size=self._data_kwargs['batch_size'],
                                               adj_mx=adj_mx, **self._model_kwargs)

        with tf.name_scope('Test'):
            with tf.variable_scope('DCRNN', reuse=True):
                self._test_model = DCRNNModel(is_training=False, scalar=scalar,
                                              batch_size=self._data_kwargs['test_batch_size'],
                                              adj_mx=adj_mx, **self._model_kwargs)

        # Learning rate.
        self._lr = tf.get_variable('learning_rate', shape=(), initializer=tf.constant_initializer(0.01),
                                   trainable=False)
        self._new_lr = tf.placeholder(tf.float32, shape=(), name='new_learning_rate')
        self._lr_update = tf.assign(self._lr, self._new_lr, name='lr_update')

        # Configure optimizer
        optimizer_name = self._train_kwargs.get('optimizer', 'adam').lower()
        epsilon = float(self._train_kwargs.get('epsilon', 1e-3))
        optimizer = tf.train.AdamOptimizer(self._lr, epsilon=epsilon)
        if optimizer_name == 'sgd':
            optimizer = tf.train.GradientDescentOptimizer(self._lr, )
        elif optimizer_name == 'amsgrad':
            optimizer = AMSGrad(self._lr, epsilon=epsilon)

        # define output dimensions
        output_dim = self._model_kwargs.get('output_dim')
        preds = self._train_model.outputs
        labels = self._train_model.labels[..., :output_dim]

        # calculate loss
        null_val = 0.
        self._loss_fn = masked_mae_loss(scalar, null_val)
        self._train_loss = self._loss_fn(preds=preds, labels=labels)

        t_vars = tf.trainable_variables()
        grads = tf.gradients(self._train_loss, t_vars)
        max_grad_norm = kwargs['train'].get('max_grad_norm', 1.)
        grads, _ = tf.clip_by_global_norm(grads, max_grad_norm)
        global_step = tf.train.get_or_create_global_step()
        self._train_op = optimizer.apply_gradients(zip(grads, t_vars), global_step=global_step, name='train_op')

        max_to_keep = self._train_kwargs.get('max_to_keep', 100)
        self._epoch = 0
        self._saver = tf.train.Saver(tf.global_variables(), max_to_keep=max_to_keep)

        # Log model statistics
        total_trainable_parameter = utils.get_total_trainable_parameter_size()
        self._logger.info('\n\nTotal number of trainable parameters in the current graph: ' +
                          '{:d}'.format(total_trainable_parameter))
        for var in tf.global_variables():
            self._logger.debug('{}, {}'.format(var.name, var.get_shape()))

    @staticmethod
    def _get_log_dir(kwargs):
        log_dir = kwargs['train'].get('log_dir')
        if log_dir is None:
            batch_size = kwargs['data'].get('batch_size')
            learning_rate = kwargs['train'].get('base_lr')
            max_diffusion_step = kwargs['model'].get('max_diffusion_step')
            num_rnn_layers = kwargs['model'].get('num_rnn_layers')
            rnn_units = kwargs['model'].get('rnn_units')
            structure = '-'.join(
                ['%d' % rnn_units for _ in range(num_rnn_layers)])
            horizon = kwargs['model'].get('horizon')
            filter_type = kwargs['model'].get('filter_type')
            filter_type_abbr = 'L'
            if filter_type == 'random_walk':
                filter_type_abbr = 'R'
            elif filter_type == 'dual_random_walk':
                filter_type_abbr = 'DR'
            base_dir = kwargs.get('base_dir')
            model_num = kwargs.get('model_number')
            model_id = 'V%s/' % model_num
            model_dir = os.path.join(base_dir, model_id)
            run_id = 'exp_%s_%d_h_%d_%s_lr_%g_bs_%d_TS_%s/' % (
                filter_type_abbr, max_diffusion_step, horizon,
                structure, learning_rate, batch_size,
                time.strftime('%Y-%m-%d_%H-%M-%S'))
            log_dir = os.path.join(model_dir, run_id)
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        return log_dir

    def run_epoch_generator(self, sess, model, data_generator, return_output=False, training=False, writer=None):
        losses = []
        outputs = []
        output_dim = self._model_kwargs.get('output_dim')
        predictions = model.outputs
        labels = model.labels[..., :output_dim]
        loss = self._loss_fn(preds=predictions, labels=labels)
        fetches = {
            'loss': loss,
            'global_step': tf.train.get_or_create_global_step()
        }
        if training:
            fetches.update({
                'train_op': self._train_op
            })
            merged = model.merged
            if merged is not None:
                fetches.update({'merged': merged})

        if return_output:
            fetches.update({
                'outputs': model.outputs
            })

        # _, : data generator iterator batch size
        for _, (x, y) in enumerate(data_generator):
            feed_dict = {
                model.inputs: x,
                model.labels: y,
            }

            # train a model
            values = sess.run(fetches, feed_dict=feed_dict)

            losses.append(values['loss'])
            if writer is not None and 'merged' in values:
                writer.add_summary(values['merged'], global_step=values['global_step'])
            if return_output:

                outputs.append(values['outputs'])

        results = {
            'loss': np.mean(losses)
        }
        if return_output:
            results['outputs'] = outputs
        return results

    def get_lr(self, sess):
        return np.asscalar(sess.run(self._lr))

    def set_lr(self, sess, lr):
        sess.run(self._lr_update, feed_dict={
            self._new_lr: lr
        })

    def train(self, sess, **kwargs):
        kwargs.update(self._train_kwargs)
        return self._train(sess, **kwargs)

    def _train(self, sess, save_model=1,
               **train_kwargs):
        history = []

        # define arrays for training and validation loss
        epoch_train_loss = []
        epoch_validate_loss = []

        # get model number for reference
        model_num = self._kwargs.get('model_number')

        min_val_loss = float('inf')
        wait = 0

        # return hyper-parameters from yaml file
        max_to_keep = train_kwargs.get('max_to_keep', 100)
        base_lr = train_kwargs.get('base_lr')
        min_learning_rate = train_kwargs.get('min_learning_rate')
        lr_decay_ratio = train_kwargs.get('lr_decay_ratio')
        test_every_n_epochs = train_kwargs.get('test_every_n_epochs')
        steps = train_kwargs.get('steps')
        epoch = train_kwargs.get('epoch')
        epochs = train_kwargs.get('epochs')
        patience = train_kwargs.get('patience')

        saver = tf.train.Saver(tf.global_variables(), max_to_keep=max_to_keep)
        model_filename = train_kwargs.get('model_filename')
        if model_filename is not None:
            saver.restore(sess, model_filename)
            self._epoch = epoch + 1
        else:
            sess.run(tf.global_variables_initializer())
        self._logger.info('\n\nStart training ...')

        #####################################################################
        #                            New Code
        #####################################################################
        # Print out Ground_Truths & Predictions by DCRNN

        # write new csv files to print out ground_truths & predictions

        # define directory for files
        data_dir = 'data_values/'
        truth_file = 'y_truth_values.csv'
        prediction_file = 'y_predicted_values.csv'
        loss_diff_file = 'y_loss_diff_values.csv'
        files = list([truth_file, prediction_file, loss_diff_file])

        # create directory
        data_values_dir = os.path.join(self._log_dir, data_dir)
        os.makedirs(data_values_dir)

        # define an index for data visualisation file
        idx = 0

        # get list of static nodes to build x-axis data frame
        with open("../data/DSB_Data/graph/rpcPairNodes_V" + str(model_num) + ".txt", "r") as nodeReader:
            node_cols = nodeReader.read().strip().split(',')

        # to build columns headers for data_file
        node_cols.insert(0, "Samples")

        # get list of timestamped windows to build data frame y-axis
        with open("../data/testing_Timestamps/test_timestamps_V" + str(model_num) + ".txt", "r") as reader:
            testing_timestamps = reader.read().strip().split(',')

        # create data frames for both actual values and predictions
        my_total_truths_df = pd.DataFrame(columns=node_cols)
        my_total_truths_df.to_csv(os.path.join(data_values_dir, truth_file),
                                  sep=',', encoding='utf-8', index_label='index')

        my_total_predictions_df = pd.DataFrame(columns=node_cols)
        my_total_predictions_df.to_csv(os.path.join(data_values_dir, prediction_file),
                                       sep=',', encoding='utf-8', index_label='index')

        my_total_loss_diff_df = pd.DataFrame(columns=node_cols)
        my_total_loss_diff_df.to_csv(os.path.join(data_values_dir, loss_diff_file),
                                       sep=',', encoding='utf-8', index_label='index')
        #####################################################################

        while self._epoch <= epochs:

            self._logger.info("\nCurrent Epoch: " + str(self._epoch))
            # Learning rate schedule.
            new_lr = max(min_learning_rate, base_lr * (lr_decay_ratio ** np.sum(self._epoch >= np.array(steps))))
            self.set_lr(sess=sess, lr=new_lr)

            start_time = time.time()
            train_results = self.run_epoch_generator(sess, self._train_model,
                                                     self._data['train_loader'].get_iterator(),
                                                     training=True,
                                                     writer=self._writer)
            train_loss = train_results['loss']
            epoch_train_loss.append(train_loss)

            if train_loss > 1e5:
                self._logger.warning('Gradient explosion detected. Ending...')
                break

            global_step = sess.run(tf.train.get_or_create_global_step())

            # Compute validation results
            val_results = self.run_epoch_generator(sess, self._test_model,
                                                   self._data['val_loader'].get_iterator(),
                                                   training=False)

            val_loss = np.asscalar(val_results['loss'])
            epoch_validate_loss.append(val_results['loss'])

            utils.add_simple_summary(self._writer,
                                     ['loss/train_loss', 'loss/val_loss'],
                                     [train_loss, val_loss], global_step=global_step)
            end_time = time.time()

            time_val = end_time - start_time
            message = 'Epoch [{}/{}] ({}) train_mae: {:.4f}, val_mae: {:.4f} lr:{:.6f} {:.2f}s'.format(
                self._epoch, epochs, global_step, train_loss, val_loss, new_lr, time_val)
            self._logger.info(message)

            # epochs where performance is evaluated, e.g. evaluate every kth epoch
            if self._epoch % test_every_n_epochs == test_every_n_epochs - 1:

                #####################################################################
                #                            My New Code
                #####################################################################
                # Print out Predictions by model

                outputs = self.evaluate(sess)

                # print out the name of the epoch
                epoch_df = pd.DataFrame()
                epoch_df.at[idx, 1] = 'Test_Epoch_%s' % str(self._epoch)

                for f in files:
                    epoch_df.to_csv(os.path.join(data_values_dir, f), mode="a", header=False)
                idx += 1

                ground_truths = outputs['ground_truth'][0]
                predictions = outputs['prediction_values'][0]
                loss_diff_values = outputs['loss_diff_values'][0]
                num_time_windows = len(testing_timestamps)

                #####################################################################
                # My New Code V3
                #####################################################################

                # define a dataframe with all testing prediction loss values returned
                my_prediction_loss = []

                # prepare a print out of actual values and predictions
                # put spaces between the time window samples and epochs

                # print out all values in all test samples (time windows set aside for evaluation)
                for s in range(0, num_time_windows):
                    # prepare data frames for actual testing values, model predictions and testing errors
                    my_actual_df = pd.DataFrame()
                    my_prediction_df = pd.DataFrame()
                    my_pred_loss_df = pd.DataFrame()

                    # append the time window to see all future predictions
                    testing_timestamp = testing_timestamps[s]
                    timestamp_df = pd.DataFrame()
                    timestamp_df.at[idx, 1] = testing_timestamp

                    for f in files:
                        timestamp_df.to_csv(os.path.join(data_values_dir, f), mode="a", header=False)

                    idx += 1

                    # output the horizon at appropriate time window
                    val = s + 1
                    horizon_val = "Horizon_%s" % val
                    my_actual_df.at[idx, 1] = horizon_val
                    my_prediction_df.at[idx, 1] = horizon_val
                    my_pred_loss_df.at[idx, 1] = horizon_val

                    # return all enumerated predictions at future time steps
                    truth = ground_truths[s]
                    pred = predictions[s]
                    loss = loss_diff_values[s]

                    # index the tensor at the appropriate time window
                    for n, (truth_val, pred_val, loss_val) in enumerate(zip(truth, pred, loss)):

                        # append the values for the nth node at specified time_window within sample
                        my_actual_df.at[idx, n + 2] = truth_val
                        my_prediction_df.at[idx, n + 2] = pred_val
                        my_pred_loss_df.at[idx, n + 2] = loss_val

                    # increment the index for data frame
                    idx += 1

                    my_actual_df.to_csv(os.path.join(data_values_dir, truth_file), mode='a', header=False)
                    my_prediction_df.to_csv(os.path.join(data_values_dir, prediction_file), mode='a', header=False)
                    my_pred_loss_df.to_csv(os.path.join(data_values_dir, loss_diff_file), mode='a', header=False)

                    # append testing prediction errors
                    my_prediction_loss.append(my_pred_loss_df)

                    # append a space between sets of horizons
                    sample_space_df = pd.DataFrame([''], index=[idx])
                    for f in files:
                        sample_space_df.to_csv(os.path.join(data_values_dir, f), mode='a', header=False)
                    idx += 1

                my_prediction_loss = pd.concat(my_prediction_loss, ignore_index=True)

                #####################################################################
                # My New Code V3
                #####################################################################

                # if self._epoch == epochs - 1:

                self.detect_anomalous_traffic(my_prediction_loss, testing_timestamps)

                # append a separator space between each epoch
                epoch_space_df = pd.DataFrame([''], index=[idx])
                for f in files:
                    epoch_space_df.to_csv(os.path.join(data_values_dir, f), mode='a', header=False)
                idx += 1
                #####################################################################
                # My New Code V2 - Ending
                #####################################################################

            if val_loss <= min_val_loss:
                wait = 0
                if save_model > 0:
                    model_filename = self.save(sess, val_loss)
                self._logger.info(
                    'Validation MAE TF loss decrease from %.8f to %.8f\n -- Saving to %s\n\n' % (min_val_loss, val_loss,
                                                                                                 model_filename))
                min_val_loss = val_loss
            else:
                wait += 1
                if wait > patience:
                    self._logger.warning('Early stopping at epoch: %d' % self._epoch)
                    self.detect_anomalous_traffic(my_prediction_loss, testing_timestamps)
                    break

            history.append(val_loss)
            # Increases epoch.
            self._epoch += 1

            sys.stdout.flush()

        # plot MAE TF Loss values in plots folder
        self.plot_eval_metrics(epoch_train_loss, epoch_validate_loss)
        # save validation loss
        # self.save_validation_loss(epoch_validate_loss)

        return np.min(history)

    def evaluate(self, sess, **kwargs):

        global_step = sess.run(tf.train.get_or_create_global_step())
        test_results = self.run_epoch_generator(sess, self._test_model,
                                                self._data['test_loader'].get_iterator(),
                                                return_output=True,
                                                training=False)

        self._logger.info("\n\nCalculating y_predictions")

        # testing loss value generated for individual epoch
        # test_predictions = list of three (test_batch_size) ndarrays (batch_size, horizon, num_nodes, output_dim)
        y_test_loss, test_predictions = test_results['loss'], test_results['outputs']
        utils.add_simple_summary(self._writer, ['loss/test_loss'], [y_test_loss], global_step=global_step)

        # y_predictions:  a list of (batch_size, horizon, num_nodes, output_dim)
        y_prediction_values = np.concatenate(test_predictions, axis=0)
        scalar = self._data['scalar']

        ground_truth_values = []
        predicted_values = []
        loss_diff_values = []

        mae_metrics = []
        mape_metrics = []
        rmse_metrics = []

        # calculate values for next horizon_i steps from current time step
        for horizon_i in range(self._data['y_test'].shape[1]):

            # self._data['y_test']
            # shape = (15, 1, 36, 1)

            # self._data['y_test'][:, horizon_i, :, 0]
            # shape of = (15, 36)

            # return the actual value for the next horizon_i step from onset
            y_truth_val = self._data['y_test'][:, horizon_i, :, 0]
            ground_truth_values.append(y_truth_val)

            # standardize the input using StandardScaler and apply invert & transform method
            # def inverse_transform(self, data):
            # return (data * self.std) + self.mean
            y_standard_truth = scalar.inverse_transform(y_truth_val)

            # return the predicted value for the next horizon_i step from onset
            y_pred_val = y_prediction_values[:y_standard_truth.shape[0], horizon_i, :, 0]
            predicted_values.append(y_pred_val)

            # standardize output using StandardScaler and apply invert & transform method
            # def inverse_transform(self, data):
            # return (data * self.std) + self.mean
            y_standard_pred = scalar.inverse_transform(y_pred_val)

            # subtract the predicted output values from actual values
            y_pred_diff = abs(y_truth_val - y_pred_val)
            loss_diff_values.append(y_pred_diff)

            # quantify the model's regression performance using the 3 metrics
            mae = metrics.masked_mae_np(y_standard_pred, y_standard_truth, null_val=0)
            mape = metrics.masked_mape_np(y_standard_pred, y_standard_truth, null_val=0)
            rmse = metrics.masked_rmse_np(y_standard_pred, y_standard_truth, null_val=0)
            self._logger.info(
                "Horizon {:02d}, MAE: {:.8f}, MAPE: {:.8f}, RMSE: {:.8f}".format(
                    horizon_i + 1, mae, mape, rmse
                )
            )
            utils.add_simple_summary(self._writer,
                                     ['%s_%d' % (item, horizon_i + 1) for item in
                                      ['metric/rmse', 'metric/mape', 'metric/mae']],
                                     [rmse, mape, mae],
                                     global_step=global_step)

            mae_metrics.append(mae)
            mape_metrics.append(mape)
            rmse_metrics.append(rmse)

        outputs = {
            'ground_truth': ground_truth_values,
            'prediction_values': predicted_values,
            'loss_diff_values': loss_diff_values,
            'mae_metrics_np': mae_metrics,
            'mape_metrics_np': mape_metrics,
            'rmse_metrics_np': rmse_metrics
        }
        return outputs

    def load(self, sess, model_filename):
        """
        Restore from saved model.
        :param sess:
        :param model_filename:
        :return:
        """
        self._saver.restore(sess, model_filename)

    def save(self, sess, val_loss):
        config = dict(self._kwargs)
        global_step = np.asscalar(sess.run(tf.train.get_or_create_global_step()))
        prefix = os.path.join(self._log_dir, 'models-{:.4f}'.format(val_loss))
        config['train']['epoch'] = self._epoch
        config['train']['global_step'] = global_step
        config['train']['log_dir'] = self._log_dir
        config['train']['model_filename'] = self._saver.save(sess,
                                                             prefix,
                                                             global_step=global_step,
                                                             write_meta_graph=False)
        config_filename = 'config_{}.yaml'.format(self._epoch)
        with open(os.path.join(self._log_dir, config_filename), 'w') as f:
            yaml.dump(config, f, default_flow_style=False)
        return config['train']['model_filename']

    def plot_eval_metrics(self, training_loss, validating_loss):

        # define and create a MAE metric directory
        loss_plot_dir = 'mae_loss_plots/'
        loss_dir = os.path.join(self._log_dir, loss_plot_dir)
        os.makedirs(loss_dir)

        # define a metric file
        loss_file = 'mae_plot_file.png'

        plt.figure()
        plt.plot(training_loss, label='training_loss')
        plt.plot(validating_loss, label='validating loss')
        plt.title('Plot MAE loss value for DeathStarBench Data using DCRNN')
        plt.xlabel('epoch')
        plt.ylabel('Loss')
        plt.legend(loc='upper right')
        plt.savefig(os.path.join(loss_dir, loss_file))
        plt.show()

    #####################################################################
    # My New Code V3
    #####################################################################

    def detect_anomalous_traffic(self, testing_error_df, test_timestamps):

        """
        calculate the mean and standard deviation over every node entry within each time window
        and set respective thresholds for every node

        Search for anomalous nodes at time window at time of cyber attack intrusion
        """

        # get the model number and number of nodes from trainable parameters
        model_num = self._kwargs.get('model_number')
        num_nodes = self._model_kwargs.get('num_nodes')

        # define directory and filename for files detecting anomalies
        anomaly_dir = 'data_anomalies/'
        anomaly_file = 'anomalous_values.txt'

        # create directory for the anomaly file
        data_anomalies_dir = os.path.join(self._log_dir, anomaly_dir)
        if not os.path.exists(data_anomalies_dir):
            os.makedirs(data_anomalies_dir)

        # list of mean values for every node over all time windows
        node_mean_list = list()

        # list of std_dev values for each node over all time windows
        node_stddev_list = list()

        # list of tuples containing upper and lower traffic limits
        node_thresh_limits = list()

        # get list of static nodes to build data frame
        with open("../data/DSB_Data/graph/rpcPairNodes_V" + str(model_num) + ".txt", "r") as nodeReader:
            nodes = nodeReader.read().strip().split(',')

        # get static node span operation names in new data frame
        static_node_pairs = pd.read_csv('../data/rpcNodePairLists/myStaticRPCNodeList_V'+str(model_num)+'.csv',
                                        encoding='latin-1', sep=',', keep_default_na=False)

        # get dictionary of source, destination pair nodes for every node identifier
        node_dict = {}
        for i, row in static_node_pairs.iterrows():

            # append node pair to respective node IDs and store in dict
            node_tuple = (row['source_call'], row['destination_call'])
            node_dict[row['nodeID']] = node_tuple

        # generate a dictionary of lists of values at every time window for each node
        list_dict = {}
        for c in range(len(testing_error_df.columns) - 1):

            # get a list of values for every existing node 'c'
            # and appends to a list for each node
            node_list = list()
            for i, row in testing_error_df.iterrows():

                # store values for the node 'c' at every 'i' time step into list
                val = c + 1
                res = row.iloc[val]
                node_list.append(res)

            # assign list of node 'c' values to respective node ID in dict
            list_dict[nodes[c]] = node_list

            # find the average value of a list of node values
            mean_value = sum(node_list) / len(node_list)
            node_mean_list.append(mean_value)

            # find the stddev of a list of node values
            variance = sum([(i - mean_value) ** 2 for i in node_list]) / len(node_list)
            stddev_result = math.sqrt(variance)
            node_stddev_list.append(stddev_result)

            # find the threshold value for node 'c'

            # find traffic limits using mean +/- (2 * std)
            # find traffic limits using mean +/- (3 * std)
            traffic_threshold = mean_value + (2 * stddev_result)
            node_thresh_limits.append(traffic_threshold)

        with open(os.path.join(data_anomalies_dir, anomaly_file), 'w') as anomalyFile:
            anomalyFile.write("Model Number: " + str(model_num))
            anomalyFile.write("\n\nTesting dataFrame shape: " + str(testing_error_df.shape))
            anomalyFile.write("\nNo. of time windows: " + str(testing_error_df.shape[0]))
            anomalyFile.write("\nNo. of nodes: " + str(num_nodes)+"\n")
            anomalyFile.write('*'*200)
            anomalyFile.write("\n\nAll node values at every time window\n")
            list_idx = 1

            # iterate through nodes ID, mean and std val, thresholds for every node c
            for (key, l_list, m_value, std_value, t_threshold) in \
                    zip(list_dict.keys(), list_dict.values(), node_mean_list, node_stddev_list, node_thresh_limits):
                anomalyFile.write("Node %s " % list_idx+"\n")
                anomalyFile.write("Node ID: " + str(key) + "\n")
                anomalyFile.write("Node pair of spans: " + str(node_dict[key]) + "\n")
                anomalyFile.write("Values at Time Windows: \n\n")
                for t, value in enumerate(l_list):
                    anomalyFile.write(str(test_timestamps[t]))
                    anomalyFile.write(":  "+str(value)+"\n")
                anomalyFile.write("\nMean: " + str(m_value) + "\n")
                anomalyFile.write("Std Dev: " + str(std_value) + "\n")
                anomalyFile.write("Threshold Limit (mean + ("+str(2)+" * std)):  ")
                anomalyFile.write(str(t_threshold) + "\n")
                anomalyFile.write("\nAnomalous timestamps?\n")

                # check if anomaly exists for a single node
                for j, val in enumerate(l_list):
                    if val > t_threshold:
                        anomalyFile.write('*' * 25)
                        anomalyFile.write("\nANOMALY DETECTED!\n")
                        anomalyFile.write("\nIndex of timestamp anomaly:  "+str(j))
                        anomalyFile.write("\nTimestamp of anomaly:  "+str(test_timestamps[j])+"\n")
                        anomalyFile.write(str(val) + "\n\n")
                        self._threshold = t_threshold

                anomalyFile.write('-' * 150)
                anomalyFile.write("\n\n")
                list_idx += 1

        ##########################################################################################
        # My New Code - Displaying RPC traffic at time window of cyber attack on .png file
        ##########################################################################################
        idx = 2

        my_anomalous_node_df = pd.DataFrame(columns=['NodeID', 'Node', 'Prediction Error', 'Threshold'])

        for k, v in list_dict.items():
            my_anomalous_node_df.at[idx, 'NodeID'] = k
            my_anomalous_node_df.at[idx, 'Node'] = node_dict[k]
            my_anomalous_node_df.at[idx, 'Prediction Error'] = v[-1]
            my_anomalous_node_df.at[idx, 'Threshold'] = self._threshold
            idx += 1
        my_anomalous_node_df.to_csv(os.path.join(data_anomalies_dir, "brute_force_project_data.csv"),
                                    sep=',', encoding='utf-8', index_label='Index')

    # saving validation loss to csv file
    def save_validation_loss(self, validation_mae_loss):

        validate_loss_df = pd.DataFrame(columns=['nodeID', 'Validation_loss'])

        idx = 1
        for i in validation_mae_loss:

            validate_loss_df = validate_loss_df.append({
                'nodeID': idx,
                'Validation_loss': i},
                ignore_index=True)
            idx += 1
        print(idx)

        validate_loss_df.to_csv('../../myBruteForceRPCGraph_Proj/data/validation_Loss.csv',
                                sep=',', encoding='utf-8')






