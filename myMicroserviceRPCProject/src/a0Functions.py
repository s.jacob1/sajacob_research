from datetime import datetime


# define a function to return a model version number
def get_model_number():
    model_number = 20
    return model_number


def convert_func(val):

    val = int(val)
    d_value = datetime.fromtimestamp(float(val / 1000000))
    return d_value
