import pandas as pd

# import functions to read model number
from src.a0Functions import get_model_number


# define a function to remove duplicate tuples from a list
def get_rpc_node_list(model_num):

    # return a dataframe containing all spanIDs and dictionary for all different RPC types
    my_span_list_df = pd.read_csv('../data/mySpanDataDF_V'+str(model_num)+'.csv',
                                  encoding='latin-1', sep=',', keep_default_na=False)

    # iterate through data frame
    # gather all unique microservice rpc pairs (src, dst)
    rpc_df = pd.DataFrame(columns=['nodeID', 'source', 'destination', 'source_call', 'destination_call'])
    rpc_node_dict = {}

    destinations = list(my_span_list_df['destination'].unique())

    # iterate through dataframe twice to discover src and dst pairs
    for i, row in my_span_list_df.iterrows():

        print("i:", str(i))
        # find first span of every trace
        # first node of tuple will have a null value for source
        if row['source'] == '':

            null_tuple = (0, row['rpcNumber'])
            if null_tuple not in rpc_node_dict.values():
                rpc_node_dict[row['destination']] = null_tuple
                rpc_df = rpc_df.append({
                    'nodeID': str(row['destination']),
                    'source': '0',
                    'destination': str(row['rpcNumber']),
                    'source_call': '',
                    'destination_call': row['rpcCall']},
                                     ignore_index=True)
                continue

        # check for traces with no parent (source) span (see Jaeger and csv file)
        if row['source'] not in destinations:

            start_tuple = (0, row['rpcNumber'])

            if start_tuple not in rpc_node_dict.values():
                rpc_node_dict[row['destination']] = start_tuple
                rpc_df = rpc_df.append({
                    'nodeID': str(row['destination']),
                    'source': '0',
                    'destination': str(row['rpcNumber']),
                    'source_call': '',
                    'destination_call': str(row['rpcCall'])},
                                    ignore_index=True)
                continue

        for j, col in my_span_list_df.iterrows():

            if row['destination'] == col['source']:

                row_tuple = (row['rpcNumber'], col['rpcNumber'])
                if row_tuple not in rpc_node_dict.values():
                    rpc_node_dict[col['destination']] = row_tuple
                    rpc_df = rpc_df.append({
                        'nodeID': str(col['destination']),
                        'source': row['rpcNumber'],
                        'destination': col['rpcNumber'],
                        'source_call': row['rpcCall'],
                        'destination_call': col['rpcCall']},
                                         ignore_index=True)
                    continue

    node_ids_list = list(rpc_node_dict.keys())
    node_dim = len(node_ids_list)

    return rpc_df, node_ids_list, node_dim


model_number = get_model_number()


rpcNodeDF, nodeIDsList, n_nodes = get_rpc_node_list(model_number)

# set the index column to nodeID
rpcNodeDF = rpcNodeDF.set_index('nodeID')
rpcNodeDF.to_csv('../data/rpcNodePairLists/myStaticRPCNodeList_V'+str(model_number)+'.csv',
                 sep=',', encoding='utf-8')

# append node IDS to model data
with open("../data/rpcNode_Trace_Info/rpcNodeInfo_V"+str(model_number)+".txt", "a") as file:
    file.write("\n\nNumber of different nodes: %s" % n_nodes)
    file.write("\n\nList of RPC nodes:\n")
    for node in nodeIDsList:
        file.write("%s\n" % node)


# write all node ids to a .txt file
with open("../data/DSB_Data/graph/rpcPairNodes_V"+str(model_number)+".txt", 'w') as f:
    for nodeID in nodeIDsList:
        f.write("%s," % nodeID)
