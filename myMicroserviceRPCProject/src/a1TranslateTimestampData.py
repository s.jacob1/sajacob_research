import pandas as pd
from src.a0Functions import get_model_number, convert_func


def translate_timestamp_data():

    # get model number
    model_num = get_model_number()

    # use Pandas library to read fields from csv file into Data Frame
    # users can name and number experiment and model numbers at their discretion
    filename = "../data/bruteForceAttackData_V"+str(model_num)+".csv"
    # filename = "../data/bruteForceAttackData_V"+str(model_num)+".csv"
    # filename = "../data/bruteForceAttackData_V"+str(model_num)+".csv"

    pd.options.mode.chained_assignment = None  # default='warn'
    my_data_frame = pd.read_csv(filename, encoding='latin-1', sep=',')

    my_time_values = []
    # convert microseconds temporal value to actual timestamp
    for i, row in my_data_frame['startTime'].items():
        time_val = convert_func(row)
        my_time_values.append(time_val)

    # assign timestamp values to new column
    my_data_frame['timeStamp'] = my_time_values
    model_num = get_model_number()

    # replace ending of file with different name to show timestamp
    out_filename = filename.replace("_V"+str(model_num)+".csv", "_TS_V"+str(model_num)+".csv")

    # write updated dataframe to new csv file
    my_data_frame.to_csv(out_filename, sep=',', encoding='utf-8', index=False)


translate_timestamp_data()
